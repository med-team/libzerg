libzerg (1.0.7-11) unstable; urgency=medium

  * Team Upload.

  [ Steffen Moeller ]
  * d/u/metadata: yamllint

  [ Nilesh Patra ]
  * Modify compiling command, use AUTOPKGTEST_TMP
  * Add Build-Depend on libzerg0-dev in symbols file
  * Fix URLs
  * Drop compat, switch to debhelper-compat
  * Add "Rules-Requires-Root:no"
  * Bump standards version to 4.5.0

  [ Andreas Tille ]
  * Remove trailing whitespace in debian/copyright (routine-update)
  * Add salsa-ci file (routine-update)
  * Set upstream metadata fields: Archive, Repository.
  * Remove obsolete field Contact from debian/upstream/metadata (already
    present in machine-readable debian/copyright).

 -- Nilesh Patra <npatra974@gmail.com>  Mon, 02 Mar 2020 08:01:14 +0530

libzerg (1.0.7-10) unstable; urgency=medium

  * debhelper 11
  * Point Vcs fields to salsa.debian.org
  * Standards-Version: 4.2.1

 -- Andreas Tille <tille@debian.org>  Fri, 26 Oct 2018 11:15:46 +0200

libzerg (1.0.7-9) unstable; urgency=medium

  * Moved packaging from SVN to Git
  * debhelper 10
  * Priority: optional
  * Remove dbg package
  * Do not parse d/changelog
  * Standards-Version: 4.1.1
  * Fix homepage
  * d/watch: version=4, drop useless comments
  * Delete README.Debian which is just repeating reference

 -- Andreas Tille <tille@debian.org>  Tue, 28 Nov 2017 15:39:03 +0100

libzerg (1.0.7-8) unstable; urgency=medium

  * Team upload.
  * small fix - add build-essential dependency in debian/tests/control

 -- Tatiana Malygina <merlettaia@gmail.com>  Sat, 16 Jul 2016 03:42:54 +0300

libzerg (1.0.7-7) unstable; urgency=medium

  [ Tatiana Malygina ]
  * Team upload.
  * cme fix dpkg-control
  * add example files and libzerg0-dev.examples
  * add testsuite
  * mention example file debian/examples/converged.ali in debian/copyright
  * add README.test for tests and save it to docs for libzerg0-dev,
    small fixes in debian/examples/example.cpp
  * add hardening

  [ Andreas Tille ]
  * secure Vcs-Browser
  * DEP5 (proper sequence of paragraphs)
  * Drop homepage field from debian/upstream/metadata which is redundant anyway
  * debhelper 9
  * use d-shlibs

 -- Tatiana Malygina <merlettaia@gmail.com>  Sun, 26 Jun 2016 12:28:12 +0300

libzerg (1.0.7-6) unstable; urgency=medium

  * Updated standards version.
  * Corrected Vcs-Browser field.
  * Corrected location of d/upstream/metadata.

 -- Laszlo Kajan <lkajan@debian.org>  Sun, 14 Sep 2014 10:57:49 +0200

libzerg (1.0.7-5) unstable; urgency=low

  * [Andreas Tille] debian/upstream:
     - Added URL + eprint
     - Moved DOI to references
     - Make author field BibTeX compliant

  [Laszlo Kajan]
  * Removing non-policy-compliant get-orig-source target from debian/rules.

 -- Laszlo Kajan <lkajan@rostlab.org>  Fri, 06 Jul 2012 15:35:05 +0200

libzerg (1.0.7-4) unstable; urgency=low

  * Fixed svn-buildpackage issue on unstable by saving original make file
    in Makefile.libzerg-orig
  * Revised description.
  * Mention librostlab-blast as a parser option.

 -- Laszlo Kajan <lkajan@rostlab.org>  Fri, 20 Jan 2012 14:26:44 +0100

libzerg (1.0.7-3) unstable; urgency=low

  [ Laszlo Kajan ]
  * Tille: debian/upstream-metadata.yaml: Added some missing fields
    Tue, 10 Jan 2012 08:30:18 +0100
  * Kajan: Renamed libzerg-dev to libzerg0-dev, Provides: libzerg-dev
  * Renamed libzerg-dbg to libzerg0-dbg.
  * dh build now, no more CDBS, with dh-autoreconf.
  * No more debian-changes patches.

  [ Andreas Tille ]
  * debian/patches/save_makefile.patch
    - Make sure original Makefile can be restored
  * debian/rules: Fixed clean target

 -- Andreas Tille <tille@debian.org>  Wed, 18 Jan 2012 13:35:10 +0100

libzerg (1.0.7-2) unstable; urgency=low

  * Patched lexer to handle blastpgp [2.2.18] output.

 -- Laszlo Kajan <lkajan@rostlab.org>  Tue, 20 Sep 2011 10:51:18 +0200

libzerg (1.0.7-1) unstable; urgency=low

  * Initial release (Closes: #641581)
  * Pached to build with autotools and libtools allowing for a shared library
    as well as the static

 -- Laszlo Kajan <lkajan@rostlab.org>  Wed, 14 Sep 2011 15:29:37 +0200
